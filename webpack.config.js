const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const port = 3000;
const nodeExternals = require('webpack-node-externals');
const WebpackAssetsManifest = require('webpack-assets-manifest');

module.exports = {
    mode: 'development',
    resolve: {
        fallback: {
            fs: false,
        }
    },
    entry: {
        app: path.resolve(
            __dirname,
            'app',
            'index.js'
        ),
        vm: path.resolve(
            __dirname,
            'node_modules',
            '@casual-simulation',
            'aux-vm-browser',
            'html',
            'IframeEntry.js'
        ),
        worker: path.resolve(
            __dirname,
            'node_modules',
            '@casual-simulation',
            'aux-vm-browser',
            'vm',
            'WorkerEntry.js'
        ),

    },
    output: {
        publicPath: '',
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].[hash].js'
    },

    // resolve: {
    //     extensions: [".ts", ".js"],
    //     fallback: {
    //         "child_process": false,
    //         "crypto": false,
    //         "path": false,
    //         "fs": false,
    //         "os": false,
    //         "tty": false,
    //         // and also other packages that are not found
    //     }
    // },
    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },

            {
                test: /\.css$/,
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true,
                            sourceMap: true
                        }
                    }
                ]
            },
            {
                test: /\.(wasm)$/,
                loader: 'file-loader',

            },
            {
                test: /\.worker(\.(ts|js))?$/,
                use: [
                    {
                        loader: 'worker-loader',
                        // loader: path.resolve(
                        //     __dirname,
                        //     'node_modules',
                        //     '@casual-simulation',
                        //     '../loaders/worker-loader/cjs.js'
                        // ),
                        options: {
                            inline: 'fallback',
                        },
                    },
                ],
               
            },

        ]
    },
    plugins: [
        new webpack.NormalModuleReplacementPlugin(/^esbuild$/, 'esbuild-wasm'),
        // new webpack.DefinePlugin({
        //     'process.env.NODE_ENV': JSON.stringify('development')
        // }),
        new WebpackAssetsManifest(),

        new webpack.ProvidePlugin({
            process: 'process/browser.js',
        }),
        new HtmlWebpackPlugin({
            chunks: ['app'],
            template: path.resolve(
                __dirname,
                'app',
                'index.html'
            ),
        }),
        new HtmlWebpackPlugin({
            chunks: ['vm'],
            template: path.resolve(
                __dirname,
                'iframe_host.html'
            ),
            title: 'AUX VM',
            filename: 'aux-vm-iframe.html',
        }),

    ],
    devServer: {
        host: 'localhost',
        port: port,
        historyApiFallback: true,
    }
};

